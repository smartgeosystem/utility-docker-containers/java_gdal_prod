# Java GDAL Prod

Container for exec Java projects with GDAL deps.   

Ubuntu 20.04 based:
- Java 8  
- Java 11  

Ubuntu 22.04 based:
- Java 8  
- Java 18  
- Liberica Java 11

Debian 11.5 (bullseye) based:
- Java 11
- Java 17
- Liberica Java 11


Packages:

- Java JRE  
- ca-certificates  
- GDAL bin

